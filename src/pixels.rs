use std::{
    cell::{Cell, RefCell},
    sync::mpsc::{self, Receiver, Sender},
};

use arrayvec::ArrayVec;
use gstreamer::{element_error, prelude::*};
use gtk4::{glib, prelude::*, subclass::prelude::*};

use crate::{HEIGHT, WIDTH};

mod imp {
    use super::*;

    type State = ArrayVec<bool, { WIDTH * HEIGHT }>;

    pub struct Pixels {
        checks: RefCell<[gtk4::CheckButton; WIDTH * HEIGHT]>,
        pipeline: RefCell<gstreamer::Pipeline>,
        sender: Cell<Option<Sender<State>>>,
        receiver: Receiver<State>,
    }

    impl Default for Pixels {
        fn default() -> Self {
            let pipeline =
                gstreamer::parse_launch(&format!("playbin uri=file://{}/scale.mp4", env!("PWD")))
                    .expect("Build playbin")
                    .downcast::<gstreamer::Pipeline>()
                    .expect("Fetch pipelin");
            let (sender, receiver) = mpsc::channel();
            Self {
                checks: RefCell::new(array_init::array_init(|_| gtk4::CheckButton::new())),
                pipeline: RefCell::new(pipeline),
                sender: Cell::new(Some(sender)),
                receiver,
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Pixels {
        const NAME: &'static str = "ZPixels";

        type Type = super::Pixels;
        type ParentType = gtk4::Grid;
        type Interfaces = ();
    }

    impl ObjectImpl for Pixels {
        fn constructed(&self) {
            self.parent_constructed();

            let checks = self.checks.borrow();

            for row in 0..HEIGHT {
                for col in 0..WIDTH {
                    self.instance().attach(
                        &checks[(row * WIDTH) + col],
                        col as i32,
                        row as i32,
                        1,
                        1,
                    );
                }
            }

            self.instance().add_tick_callback(|w, c| w.tick(c));

            let pipeline = self.pipeline.borrow();

            let videoconvert = gstreamer::ElementFactory::make("autovideoconvert")
                .build()
                .expect("Build converter");
            let appsink = gstreamer_app::AppSink::builder()
                .caps(
                    &gstreamer_video::VideoCapsBuilder::new()
                        .width(80)
                        .height(60)
                        .build(),
                )
                .build();
            let sinkbin = gstreamer::Bin::new(Some("check_bin"));
            sinkbin
                .add_many(&[&videoconvert, appsink.upcast_ref()])
                .expect("Populate bin");
            gstreamer::Element::link_many(&[&videoconvert, appsink.upcast_ref()])
                .expect("Link apple elements");

            let pad = videoconvert.static_pad("sink").expect("Fetch sink");
            let ghost = gstreamer::GhostPad::with_target(Some("sink"), &pad).expect("Make ghost");
            ghost.set_active(true).expect("Make ghost active");
            sinkbin.add_pad(&ghost).expect("Add ghost");

            pipeline.set_property("video-sink", sinkbin);

            let sender = self.sender.take().expect("Steal sender");

            appsink.set_callbacks(
                gstreamer_app::AppSinkCallbacks::builder()
                    .new_sample(move |appsink| Pixels::got_sample(&sender, appsink))
                    .build(),
            );
        }

        fn dispose(&self) {
            let pipeline = self.pipeline.borrow();
            pipeline
                .set_state(gstreamer::State::Null)
                .expect("Stop pipeline");
            pipeline
                .bus()
                .expect("Fetch bus")
                .remove_watch()
                .expect("Stop watching");
        }
    }

    impl WidgetImpl for Pixels {}
    impl GridImpl for Pixels {}

    impl Pixels {
        pub(super) fn start(&self) {
            let pipeline = self.pipeline.borrow();
            let bus = pipeline.bus().unwrap();

            bus.add_watch_local(move |_, msg| {
                match msg.view() {
                    gstreamer::MessageView::Eos(..) => {
                        println!("End")
                    }
                    gstreamer::MessageView::Error(err) => {
                        println!(
                            "Error from {:?}: {} ({:?})",
                            err.src().map(|s| s.path_string()),
                            err.error(),
                            err.debug()
                        );
                    }
                    _ => (),
                };

                gtk4::glib::Continue(true)
            })
            .expect("Start watching");

            pipeline
                .set_state(gstreamer::State::Playing)
                .expect("Start pipeline");
        }

        pub(super) fn tick(&self, _clock: &gtk4::gdk::FrameClock) -> glib::Continue {
            // Should sync something, to something, or something

            if let Ok(state) = self.receiver.try_recv() {
                let checks = self.checks.borrow();
                for (i, pixel) in state.iter().enumerate() {
                    checks[i].set_active(*pixel);
                }
            }

            glib::Continue(true)
        }

        fn got_sample(
            sender: &Sender<State>,
            sink: &gstreamer_app::AppSink,
        ) -> Result<gstreamer::FlowSuccess, gstreamer::FlowError> {
            let sample = sink.pull_sample().map_err(|_| gstreamer::FlowError::Eos)?;

            let buffer = sample.buffer().ok_or_else(|| {
                element_error!(
                    sink,
                    gstreamer::ResourceError::Failed,
                    ("Failed to load buffer")
                );

                gstreamer::FlowError::Error
            })?;

            let map = buffer.map_readable().map_err(|_| {
                element_error!(
                    sink,
                    gstreamer::ResourceError::Failed,
                    ("Failed get readable")
                );

                gstreamer::FlowError::Error
            })?;

            sender
                .send(
                    map[..(WIDTH * HEIGHT)]
                        .iter()
                        .map(|pixel| !*pixel > 128)
                        .collect(),
                )
                .expect("Send frame");

            Ok(gstreamer::FlowSuccess::Ok)
        }
    }
}

gtk4::glib::wrapper! {
    pub struct Pixels(ObjectSubclass<imp::Pixels>) @extends gtk4::Grid, gtk4::Widget;
}

impl Pixels {
    pub fn new() -> Self {
        gtk4::glib::Object::new(&[])
    }

    pub fn start(&self) {
        self.imp().start();
    }

    fn tick(&self, clock: &gtk4::gdk::FrameClock) -> glib::Continue {
        self.imp().tick(clock)
    }
}
