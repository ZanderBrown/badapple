use std::error::Error;

use gtk4::prelude::*;

mod pixels;

const WIDTH: usize = 80;
const HEIGHT: usize = 60;
const CSS: &[u8] = include_bytes!("style.css");

fn activate(app: &gtk4::Application) {
    if let Some(win) = app.active_window() {
        win.present();
        return;
    }

    let win = gtk4::ApplicationWindow::new(app);
    let css_provider = gtk4::CssProvider::new();
    css_provider.load_from_data(CSS);
    if let Some(display) = gtk4::gdk::Display::default() {
        gtk4::StyleContext::add_provider_for_display(
            &display,
            &css_provider,
            gtk4::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
    }

    let pixels = pixels::Pixels::new();
    win.set_child(Some(&pixels));

    win.present();

    pixels.start();
}

fn main() -> Result<(), Box<dyn Error>> {
    gstreamer::init()?;

    let app = gtk4::Application::new(
        Some("uk.zbrown.Apple"),
        gtk4::gio::ApplicationFlags::FLAGS_NONE,
    );

    app.connect_activate(activate);

    app.run();

    Ok(())
}
